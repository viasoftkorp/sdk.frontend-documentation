SDK-Frontend-Documentation
==========

URL: https://sdk-frontend-documentation.korp.com.br/en/latest/

How to build locally
====================

1. Download Python latest version from https://www.python.org/downloads/
2. Install Python (please **CHECK to create Windows PATH**)
3. Install Sphinx using `pip install -U sphinx`
3. Install Sphinx Theme using `pip install sphinx-rtd-theme`
4. Run `sphinx-build -b html docs dist`
5. Distribute dist folder in a http server (you can use https://www.npmjs.com/package/http-server for example )

How to Live Preview
===================

1. Download `Esbonio Extension <https://marketplace.visualstudio.com/items?itemName=swyddfa.esbonio>`_ in VSCode
2. Follow the installation instructions