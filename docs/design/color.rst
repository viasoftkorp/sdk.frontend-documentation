.. role:: raw-html-m2r(raw)
   :format: html


Color
=====

Content in this page is based on `Adobe Spectrum <https://spectrum.adobe.com/page/color/>`_.

Usage
-----

``400`` should always be used as the primary color in any situation. Any other color stop should only be used if absolutely necessary.

Semantic colors
---------------

Semantic colors have assigned meanings and are used to set expectations of meaning for users. Color is used sparingly and intentionally to reinforce hierarchies and to create clear modes of communication. Too much color can create cognitive overload, affecting users' ability to efficiently interact with products.


.. raw:: html

   <div style="
     display: flex;
     flex-direction: row;
     font-family: inherit;
     text-transform: uppercase;
     font-weight: 700;
     color: rgba(255, 255, 255, 0.9);
   ">
     <!-- BLUE -->
     <div style="
       flex: 1;
       display: flex;
       flex-direction: column;
       margin: 8px;
     ">
       <div style="
         background-color: #2680eb;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
         min-height: 96px;
         border-radius: 4px 4px 0 0;
         color: white;
       ">
         <span style="font-size: 12px;">BLUE 400</span>
         <span style="font-size: 16px;">#2680eb</span>
         <span style="font-size: 10px;">Informative</span>
       </div>
       <div style="
         background-color: #1473e6;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
       ">
         <span style="font-size: 11px;">BLUE 500</span>
         <span style="font-size: 14px;">#1473e6</span>
       </div>
       <div style="
         background-color: #0d66d0;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
       ">
         <span style="font-size: 11px;">BLUE 600</span>
         <span style="font-size: 14px;">#0d66d0</span>
       </div>
       <div style="
         background-color: #095aba;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
         border-radius: 0 0 4px 4px;
       ">
         <span style="font-size: 11px;">BLUE 700</span>
         <span style="font-size: 14px;">#095aba</span>
       </div>
     </div>
     <!-- RED -->
     <div style="
       flex: 1;
       display: flex;
       flex-direction: column;
       margin: 8px;
     ">
       <div style="
         background-color: #e34850;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
         min-height: 96px;
         border-radius: 4px 4px 0 0;
         color: white;
       ">
         <span style="font-size: 12px;">RED 400</span>
         <span style="font-size: 16px;">#e34850</span>
         <span style="font-size: 10px;">Negative</span>
       </div>
       <div style="
         background-color: #d7373f;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
       ">
         <span style="font-size: 11px;">RED 500</span>
         <span style="font-size: 14px;">#d7373f</span>
       </div>
       <div style="
         background-color: #c9252d;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
       ">
         <span style="font-size: 11px;">RED 600</span>
         <span style="font-size: 14px;">#c9252d</span>
       </div>
       <div style="
         background-color: #bb121a;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
         border-radius: 0 0 4px 4px;
       ">
         <span style="font-size: 11px;">RED 700</span>
         <span style="font-size: 14px;">#bb121a</span>
       </div>
     </div>
     <!-- ORANGE -->
     <div style="
       flex: 1;
       display: flex;
       flex-direction: column;
       margin: 8px;
     ">
       <div style="
         background-color: #e68619;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
         min-height: 96px;
         border-radius: 4px 4px 0 0;
         color: white;
       ">
         <span style="font-size: 12px;">ORANGE 400</span>
         <span style="font-size: 16px;">#e68619</span>
         <span style="font-size: 10px;">Notice</span>
       </div>
       <div style="
         background-color: #da7b11;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
       ">
         <span style="font-size: 11px;">ORANGE 500</span>
         <span style="font-size: 14px;">#da7b11</span>
       </div>
       <div style="
         background-color: #cb6f10;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
       ">
         <span style="font-size: 11px;">ORANGE 600</span>
         <span style="font-size: 14px;">#cb6f10</span>
       </div>
       <div style="
         background-color: #bd640d;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
         border-radius: 0 0 4px 4px;
       ">
         <span style="font-size: 11px;">ORANGE 700</span>
         <span style="font-size: 14px;">#bd640d</span>
       </div>
     </div>
     <!-- GREEN -->
     <div style="
       flex: 1;
       display: flex;
       flex-direction: column;
       margin: 8px;
     ">
       <div style="
         background-color: #2d9d78;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
         min-height: 96px;
         border-radius: 4px 4px 0 0;
         color: white;
       ">
         <span style="font-size: 12px;">GREEN 400</span>
         <span style="font-size: 16px;">#2d9d78</span>
         <span style="font-size: 10px;">Positive</span>
       </div>
       <div style="
         background-color: #268e6c;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
       ">
         <span style="font-size: 11px;">GREEN 500</span>
         <span style="font-size: 14px;">#268e6c</span>
       </div>
       <div style="
         background-color: #12805c;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
       ">
         <span style="font-size: 11px;">GREEN 600</span>
         <span style="font-size: 14px;">#12805c</span>
       </div>
       <div style="
         background-color: #107154;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
         border-radius: 0 0 4px 4px;
       ">
         <span style="font-size: 11px;">GREEN 700</span>
         <span style="font-size: 14px;">#107154</span>
       </div>
  </div>

Label colors
------------

Label colors have no semantic meaning attached to them. They can be used to show relationships between content types (e.g., categorization, labels in data visualizations).


.. raw:: html

  <div style="
    display: flex;
    flex-direction: row;
    font-family: 'Segoe UI', sans-serif;
    text-transform: uppercase;
    font-weight: 700;
    color: rgba(255, 255, 255, 0.9);
  ">

     <!-- INDIGO -->
     <div style="
       flex: 1;
       display: flex;
       flex-direction: column;
       margin: 8px;
     ">
       <div style="
         background-color: #6767ec;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
         min-height: 64px;
         border-radius: 4px 4px 0 0;
         color: white;
       ">
         <span style="font-size: 12px;">INDIGO 400</span>
         <span style="font-size: 16px;">#6767ec</span>
       </div>
       <div style="
         background-color: #5c5ce0;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
       ">
         <span style="font-size: 11px;">INDIGO 500</span>
         <span style="font-size: 14px;">#5c5ce0</span>
       </div>
       <div style="
         background-color: #5151d3;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
       ">
         <span style="font-size: 11px;">INDIGO 600</span>
         <span style="font-size: 14px;">#5151d3</span>
       </div>
       <div style="
         background-color: #4646c6;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
         border-radius: 0 0 4px 4px;
       ">
         <span style="font-size: 11px;">INDIGO 700</span>
         <span style="font-size: 14px;">#4646c6</span>
       </div>
     </div>
   

  
    <!-- CELERY -->
    <div style="
      flex: 1;
      display: flex;
      flex-direction: column;
      margin: 8px;
    ">
    <div style="
      background-color: #44b556;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
      min-height: 64px;
      border-radius: 4px 4px 0 0;
      color: white;
    ">
      <span style="font-size: 12px;">CELERY 400</span>
      <span style="font-size: 16px;">#44b556</span>
    </div>
    <div style="
      background-color: #3da74e;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
    ">
      <span style="font-size: 11px;">CELERY 500</span>
      <span style="font-size: 14px;">#3da74e</span>
    </div>
    <div style="
      background-color: #379947;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
    ">
      <span style="font-size: 11px;">CELERY 600</span>
      <span style="font-size: 14px;">#379947</span>
    </div>
    <div style="
      background-color: #318b40;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
      border-radius: 0 0 4px 4px;
    ">
      <span style="font-size: 11px;">CELERY 700</span>
      <span style="font-size: 14px;">#318b40</span>
    </div>
  </div>

    <!-- MAGENTA -->
    <div style="
      flex: 1;
      display: flex;
      flex-direction: column;
      margin: 8px;
    ">
    <div style="
      background-color: #d83790;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
      min-height: 64px;
      border-radius: 4px 4px 0 0;
      color: white;
    ">
      <span style="font-size: 12px;">MAGENTA 400</span>
      <span style="font-size: 16px;">#d83790</span>
    </div>
    <div style="
      background-color: #ce2783;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
    ">
      <span style="font-size: 11px;">MAGENTA 500</span>
      <span style="font-size: 14px;">#ce2783</span>
    </div>
    <div style="
      background-color: #bc1c74;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
    ">
      <span style="font-size: 11px;">MAGENTA 600</span>
      <span style="font-size: 14px;">#bc1c74</span>
    </div>
    <div style="
      background-color: #ae0e66;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
      border-radius: 0 0 4px 4px;
    ">
      <span style="font-size: 11px;">MAGENTA 700</span>
      <span style="font-size: 14px;">#ae0e66</span>
    </div>
  </div>

    <!-- YELLOW -->
    <div style="
      flex: 1;
      display: flex;
      flex-direction: column;
      margin: 8px;
    ">
      <div style="
        background-color: #dfbf00;
        flex: 1;
        display: flex;
        flex-direction: column;
        padding: 8px;
        min-height: 64px;
        border-radius: 4px 4px 0 0;
        color: white;
      ">
        <span style="font-size: 12px;">YELLOW 400</span>
        <span style="font-size: 16px;">#dfbf00</span>
      </div>
      <div style="
        background-color: #d2b200;
        flex: 1;
        display: flex;
        flex-direction: column;
        padding: 8px;
      ">
        <span style="font-size: 11px;">YELLOW 500</span>
        <span style="font-size: 14px;">#d2b200</span>
      </div>
      <div style="
        background-color: #c4a600;
        flex: 1;
        display: flex;
        flex-direction: column;
        padding: 8px;
      ">
        <span style="font-size: 11px;">YELLOW 600</span>
        <span style="font-size: 14px;">#c4a600</span>
      </div>
      <div style="
        background-color: #b79900;
        flex: 1;
        display: flex;
        flex-direction: column;
        padding: 8px;
        border-radius: 0 0 4px 4px;
      ">
        <span style="font-size: 11px;">YELLOW 700</span>
        <span style="font-size: 14px;">#b79900</span>
      </div>
    </div>
  </div>


.. raw:: html

   <div style="
     display: flex;
     flex-direction: row;
     font-family: 'Segoe UI', sans-serif;
     text-transform: uppercase;
     font-weight: 700;
     color: rgba(255, 255, 255, 0.9);
   ">

     <!-- FUCHSIA -->
     <div style="
       flex: 1;
       display: flex;
       flex-direction: column;
       margin: 8px;
     ">
       <div style="
         background-color: #c038cc;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
         min-height: 64px;
         border-radius: 4px 4px 0 0;
         color: white;
       ">
         <span style="font-size: 12px;">FUCHSIA 400</span>
         <span style="font-size: 16px;">#c038cc</span>
       </div>
       <div style="
         background-color: #b130bd;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
       ">
         <span style="font-size: 11px;">FUCHSIA 500</span>
         <span style="font-size: 14px;">#b130bd</span>
       </div>
       <div style="
         background-color: #a228ad;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
       ">
         <span style="font-size: 11px;">FUCHSIA 600</span>
         <span style="font-size: 14px;">#a228ad</span>
       </div>
       <div style="
         background-color: #93219e;
         flex: 1;
         display: flex;
         flex-direction: column;
         padding: 8px;
         border-radius: 0 0 4px 4px;
       ">
         <span style="font-size: 11px;">FUCHSIA 700</span>
         <span style="font-size: 14px;">#93219e</span>
       </div>
    </div>

    <!-- SEAFOAM -->
    <div style="
      flex: 1;
      display: flex;
      flex-direction: column;
      margin: 8px;
    ">
    <div style="
      background-color: #1b959a;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
      min-height: 64px;
      border-radius: 4px 4px 0 0;
      color: white;
    ">
      <span style="font-size: 12px;">SEAFOAM 400</span>
      <span style="font-size: 16px;">#1b959a</span>
    </div>
    <div style="
      background-color: #16878c;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
    ">
      <span style="font-size: 11px;">SEAFOAM 500</span>
      <span style="font-size: 14px;">#16878c</span>
    </div>
    <div style="
      background-color: #0f797d;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
    ">
      <span style="font-size: 11px;">SEAFOAM 600</span>
      <span style="font-size: 14px;">#0f797d</span>
    </div>
    <div style="
      background-color: #096c6f;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
      border-radius: 0 0 4px 4px;
    ">
      <span style="font-size: 11px;">SEAFOAM 700</span>
      <span style="font-size: 14px;">#096c6f</span>
    </div>
  </div>

    <!-- CHARTREUSE -->
      <div style="
      flex: 1;
      display: flex;
      flex-direction: column;
      margin: 8px;
      ">
    <div style="
      background-color: #85d044;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
      min-height: 64px;
      border-radius: 4px 4px 0 0;
      color: white;
    ">
      <span style="font-size: 12px;">CHARTREUSE 400</span>
      <span style="font-size: 16px;">#85d044</span>
    </div>
    <div style="
      background-color: #7cc33f;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
    ">
      <span style="font-size: 11px;">CHARTREUSE 500</span>
      <span style="font-size: 14px;">#7cc33f</span>
    </div>
    <div style="
      background-color: #73b53a;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
    ">
      <span style="font-size: 11px;">CHARTREUSE 600</span>
      <span style="font-size: 14px;">#73b53a</span>
    </div>
    <div style="
      background-color: #6aa834;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
      border-radius: 0 0 4px 4px;
    ">
      <span style="font-size: 11px;">CHARTREUSE 700</span>
      <span style="font-size: 14px;">#6aa834</span>
    </div>
  </div>

    <!-- PURPLE -->
    <div style="
      flex: 1;
      display: flex;
      flex-direction: column;
      margin: 8px;
    ">
    <div style="
      background-color: #9256d9;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
      min-height: 64px;
      border-radius: 4px 4px 0 0;
      color: white;
    ">
      <span style="font-size: 12px;">PURPLE 400</span>
      <span style="font-size: 16px;">#9256d9</span>
    </div>
    <div style="
      background-color: #864ccc;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
    ">
      <span style="font-size: 11px;">PURPLE 500</span>
      <span style="font-size: 14px;">#864ccc</span>
    </div>
    <div style="
      background-color: #7a42bf;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
    ">
      <span style="font-size: 11px;">PURPLE 600</span>
      <span style="font-size: 14px;">#7a42bf</span>
    </div>
    <div style="
      background-color: #6f38b1;
      flex: 1;
      display: flex;
      flex-direction: column;
      padding: 8px;
      border-radius: 0 0 4px 4px;
    ">
      <span style="font-size: 11px;">PURPLE 700</span>
      <span style="font-size: 14px;">#6f38b1</span>
    </div>
  </div>

