
Changelog - Libraries
=====================

.. toctree:: 
    :maxdepth: 1

    administration/changelog
    authorization-management/changelog
    dashboard/changelog
    datepicker/changelog
    email/changelog
    file-provider/changelog
    text-editor/changelog
    map/changelog
    workflow/changelog