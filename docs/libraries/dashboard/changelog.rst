
Changelog - Dashboard
=====================

This changelog refers to the ``@viasoft/dashboard`` library.

6.2.0 (08/11/2021)
------------------

Dependency updates
^^^^^^^^^^^^^^^^^^

.. list-table::
   :header-rows: 1

   * - Dependency
     - Status
     - Version
   * - ``@viasoft/app-core``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/client``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/client-core``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/common``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/components``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/http``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/navigation``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/theming``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/view-template``
     - 🔃 Updated
     - ``6.3.4``


Breaking changes
^^^^^^^^^^^^^^^^


* `FRM-1858 <http://jira.korp.com.br/browse/FRM-1858>`_\ : Removed ``VS_DASHBOARD_PREFIX`` and replaced its usages with ``VS_API_PREFIX``.
