
Changelog - Datepicker
====================

This changelog refers to the ``@viasoft/datepicker`` library.

1.0.2 (17/08/2023)
------------------

Breaking Changes
^^^^^^^^^^^^^^^^

`SDKF-134 <https://korp.youtrack.cloud/issue/SDKF-134>`_ - The datepicker will now always open downwards by default. If there is insufficient space below, it will open upwards. In situations where there isn't enough space either below or above, the datepicker will still open downwards.     