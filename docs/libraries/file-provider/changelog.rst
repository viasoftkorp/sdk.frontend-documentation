
Changelog - File Provider
=========================

This changelog refers to the ``@viasoft/file-provider`` and ``@viasoft/custom-file-provider-tag`` libraries.

7.0.0 (23/03/2023)
------------------

This release is focused around updating to Angular 14.

Dependency Updates
^^^^^^^^^^^^^^^^^^

.. list-table::
   :header-rows: 1

   * - Dependency
     - Status
     - Version
     - Documentation
   * - @angular/animations
     - 🔃 Updated
     - ``^14.2.12``
     - `angular/animations <https://github.com/angular/angular#readme>`_
   * - @angular/cdk
     - 🔃 Updated
     - ``^14.2.7``
     - `angular/cdk <https://github.com/angular/components#readme>`_
   * - @angular/common
     - 🔃 Updated
     - ``^14.2.12``
     - `angular/common <https://github.com/angular/angular#readme>`_
   * - @angular/compiler
     - 🔃 Updated
     - ``^14.2.12``
     - `angular/compiler <https://github.com/angular/angular#readme>`_
   * - @angular/core
     - 🔃 Updated
     - ``^14.2.12``
     - `angular/core <https://github.com/angular/angular#readme>`_
   * - @angular/forms
     - 🔃 Updated
     - ``^14.2.12``
     - `angular/forms <https://github.com/angular/angular#readme>`_
   * - @angular/material
     - 🔃 Updated
     - ``^14.2.7``
     - `angular/material <https://github.com/angular/components#readme>`_
   * - @angular/platform-browser
     - 🔃 Updated
     - ``^14.2.12``
     - `angular/platform-browser <https://github.com/angular/angular#readme>`_
   * - @angular/platform-browser-dynamic
     - 🔃 Updated
     - ``^14.2.12``
     - `angular/platform-browser <https://github.com/angular/angular#readme>`_
   * - @angular/router
     - 🔃 Updated
     - ``^14.2.12``
     - `angular/router <https://github.com/angular/angular/tree/main/packages/router>`_
   * - @fortawesome/fontawesome-pro
     - 🔃 Updated
     - ``~6.2.1`` 
     - `fortawesome <https://fontawesome.com/v6/docs>`_
   * - @microsoft/signalr
     - 🔃 Updated
     - ``~7.0.0``
     - `microsoft/signalr <https://learn.microsoft.com/en-us/aspnet/core/release-notes/aspnetcore-7.0?view=aspnetcore-7.0>`_
   * - @ngx-translate/core
     - 🔃 Updated
     - ``~14.0.0``
     - `ngx-translate/core <https://github.com/ngx-translate/core/blob/master/README.md>`_
   * - @sentry/browser
     - 🔃 Updated
     - ``~7.28.1``
     - `sentry/browser <https://docs.sentry.io/platforms/javascript/guides/angular/>`_
   * - @viasoft/app-core
     - 🔃 Updated
     - ``7.0.0-beta.1``
     - `viasoft/app-core <https://proget.korp.com.br/feeds/viasoft-sdk-frontend/@viasoft/app-core/versions>`_
   * - @viasoft/client-core
     - 🔃 Updated
     - ``7.0.0-beta.1``
     - `viasoft/client-core <https://proget.korp.com.br/feeds/viasoft-sdk-frontend/@viasoft/client-core/versions>`_
   * - @viasoft/common
     - 🔃 Updated
     - ``7.0.0-beta.1``
     - `viasoft/common <https://proget.korp.com.br/feeds/viasoft-sdk-frontend/@viasoft/common/versions>`_
   * - @viasoft/components
     - 🔃 Updated
     - ``7.0.0-beta.1``
     - `viasoft/components <https://proget.korp.com.br/feeds/viasoft-sdk-frontend/@viasoft/components/versions>`_
   * - @viasoft/datepicker
     - 🔃 Updated
     - ``2.0.0``
     - `viasoft/datepicker <https://proget.korp.com.br/feeds/viasoft-sdk-frontend/@viasoft/datepicker/versions>`_
   * - @viasoft/http
     - 🔃 Updated
     - ``7.0.0-beta.1``
     - `viasoft/http <https://proget.korp.com.br/feeds/viasoft-sdk-frontend/@viasoft/http/versions>`_
   * - @viasoft/navigation
     - 🔃 Updated
     - ``7.0.0-beta.1``
     - `viasoft/navigation <https://proget.korp.com.br/feeds/viasoft-sdk-frontend/@viasoft/navigation/versions>`_
   * - @viasoft/theming
     - 🔃 Updated
     - ``7.0.0-beta.1``
     - `viasoft/theming <https://proget.korp.com.br/feeds/viasoft-sdk-frontend/@viasoft/theming/versions>`_
   * - @viasoft/view-template
     - 🔃 Updated
     - ``7.0.0-beta.1``
     - `viasoft/view-template <https://proget.korp.com.br/feeds/viasoft-sdk-frontend/@viasoft/view-template/versions>`_
   * - angular-imask
     - 🔃 Updated
     - ``~6.4.3``
     - `angular-imask <https://github.com/uNmAnNeR/imaskjs/tree/master/packages/angular-imask>`_
   * - angular-oauth2-oidc
     - 🔃 Updated
     - ``~14.0.1``
     - `angular-oauth2-oidc <https://manfredsteyer.github.io/angular-oauth2-oidc/docs/>`_
   * - core-js
     - 🔃 Updated
     - ``~3.26.1``
     - `core-js <https://github.com/zloirock/core-js>`_
   * - dateformat
     - 🔃 Updated
     - ``~5.0.3``
     - `dateformat <https://www.npmjs.com/package/dateformat>`_
   * - dompurify
     - 🔃 Updated
     - ``~2.4.1``
     - `dompurify <https://www.npmjs.com/package/dompurify>`_
   * - imask
     - 🔃 Updated
     - ``~6.4.3``
     - `imask <https://imask.js.org/guide.html>`_
   * - ng2-pdfjs-viewer
     - 🔃 Updated
     - ``14.0.0``
     - `ng2-pdfjs-viewer <https://github.com/intbot/ng2-pdfjs-viewer#readme>`_
   * - primeicons
     - 🔃 Updated
     - ``~6.0.1``
     - `primeicons <https://www.primefaces.org/diamond/icons.xhtml>`_
   * - primeng
     - 🔃 Updated
     - ``~14.2.2``
     - `primeng <https://primefaces.org/primeng/setup>`_
   * - rxjs
     - 🔃 Updated
     - ``~7.8.0``
     - `rxjs <https://rxjs.dev/guide/overview>`_
   * - single-spa
     - 🔃 Updated
     - ``^5.9.4``
     - `single-spa <https://single-spa.js.org>`_
   * - single-spa-angular
     - 🔃 Updated
     - ``^7.0.0``
     - `single-spa-angular <https://github.com/single-spa/single-spa-angular#readme>`_
   * - tslib
     - 🔃 Updated
     - ``~2.4.1``
     - `tslib <https://github.com/microsoft/tslib>`_
   * - ua-parser-js
     - 🔃 Updated
     - ``~1.0.32``
     - `ua-parser-js <https://github.com/faisalman/ua-parser-js>`_
   * - zone.js
     - 🔃 Updated
     - ``~0.12.0``
     - `zone.js <https://www.npmjs.com/package/zone.js?activeTab=readme>`_


.. list-table::
   :header-rows: 1

   * - Dev dependency
     - Status
     - Version
     - Documentation
   * - @angular-builders/custom-webpack
     - 🔄 Updated
     - ``14.1.0``
     - `angular-builders/custom-webpack <https://github.com/just-jeb/angular-builders#readme>`_
   * - @angular-devkit/build-angular
     - 🔄 Updated
     - ``^14.2.10``
     - `angular-devkit/build-angular <https://github.com/angular/angular-cli>`_
   * - @angular-eslint/builder
     - ✅ Added
     - ``14.4.0``
     - `angular-eslint/builder <https://github.com/angular-eslint/angular-eslint#readme>`_
   * - @angular-eslint/eslint-plugin
     - ✅ Added
     - ``14.4.0``
     - `angular-eslint/eslint-plugin <https://github.com/angular-eslint/angular-eslint#readme>`_
   * - @angular-eslint/eslint-plugin-template
     - ✅ Added
     - ``14.4.0``
     - `angular-eslint/eslint-plugin-template <https://github.com/angular-eslint/angular-eslint#readme>`_
   * - @angular-eslint/schematics
     - ✅ Added
     - ``14.4.0``
     - `angular-eslint/schematics <https://github.com/angular-eslint/angular-eslint#readme>`_
   * - @angular-eslint/template-parser
     - ✅ Added
     - ``14.4.0``
     - `angular-eslint/template-parser <https://github.com/angular-eslint/angular-eslint#readme>`_
   * - @angular/cli
     - 🔄 Updated
     - ``^14.2.10``
     - `angular/cli <https://github.com/angular/angular-cli>`_
   * - @angular/compiler-cli
     - 🔄 Updated
     - ``^14.2.12``
     - `angular/compiler-cli <https://github.com/angular/angular/tree/main/packages/compiler-cli>`_
   * - @angular/language-service
     - 🔄 Updated
     - ``^14.2.12``
     - `angular/language-service <https://github.com/angular/angular#readme>`_
   * - @types/node
     - 🔄 Updated
     - ``~18.11.17``
     - `types/node <https://github.com/DefinitelyTyped/DefinitelyTyped>`_
   * - @types/jasmine
     - 🔄 Updated
     - ``~4.3.1``
     - `types/jasmine <https://github.com/DefinitelyTyped/DefinitelyTyped/>`_
   * - @types/jasminewd2
     - 🔄 Updated
     - ``~2.0.10``
     - `types/jasminewd2 <https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/jasminewd2>`_
   * - codelyzer
     - ❌ Removed
     - 
     - `codelyzer <https://github.com/mgechev/codelyzer#readme>`_
   * - @typescript-eslint/eslint-plugin
     - ✅ Added
     - ``5.43.0``
     - `typescript-eslint/eslint-plugin <https://github.com/typescript-eslint/typescript-eslint#readme>`_
   * - @typescript-eslint/parser
     - ✅ Added
     - ``5.43.0``
     - `typescript-eslint/parser <https://github.com/typescript-eslint/typescript-eslint#readme>`_
   * - eslint
     - ✅ Added
     - ``^8.28.0``
     - `eslint <https://eslint.org>`_
   * - eslint-plugin-import
     - ✅ Added
     - ``2.27.5``
     - `eslint-plugin-import <https://github.com/import-js/eslint-plugin-import>`_
   * - eslint-plugin-jsdoc
     - ✅ Added
     - ``39.7.4``
     - `eslint-plugin-jsdoc <https://github.com/gajus/eslint-plugin-jsdoc#readme>`_
   * - eslint-plugin-prefer-arrow
     - ✅ Added
     - ``1.2.3``
     - `eslint-plugin-prefer-arrow <https://github.com/TristonJ/eslint-plugin-prefer-arrow#readme>`_
   * - jasmine-core
     - 🔄 Updated
     - ``~4.5.0``
     - `jasmine-core <https://github.com/jasmine/jasmine/>`_
   * - jasmine-spec-reporter
     - 🔄 Updated
     - ``~7.0.0``
     - `jasmine-spec-reporter <https://github.com/bcaudan/jasmine-spec-reporter>`_
   * - karma
     - 🔄 Updated
     - ``~6.4.1``
     - `karma <https://karma-runner.github.io/>`_
   * - karma-chrome-launcher
     - 🔄 Updated
     - ``~3.1.1``
     - `karma-chrome-launcher <https://github.com/karma-runner/karma-chrome-launcher>`_
   * - karma-coverage-istanbul-reporter
     - 🔄 Updated
     - ``~3.0.3``
     - `karma-coverage-istanbul <https://github.com/mattlewis92/karma-coverage-istanbul-reporter>`_
   * - karma-jasmine
     - 🔄 Updated
     - ``~5.1.0``
     - `karma-jasmine <https://github.com/karma-runner/karma-jasmine>`_
   * - karma-jasmine-html-reporter
     - 🔄 Updated
     - ``~2.0.0``
     - `karma-jasmine-html-reporter <https://github.com/dfederm/karma-jasmine-html-reporter>`_
   * - ng-packagr
     - 🔄 Updated
     - ``~14.2.2``
     - `ng-packagr <https://github.com/ng-packagr/ng-packagr>`_
   * - protractor
     - ❌ Removed
     - 
     - `protractor <https://github.com/angular/protractor>`_
   * - ts-node
     - 🔄 Updated
     - ``~10.9.1``
     - `ts-node <https://typestrong.org/ts-node/>`_
   * - tslint
     - ❌ Removed
     - 
     - `tslint <https://palantir.github.io/tslint>`_
   * - typescript
     - 🔄 Updated
     - ``~4.7.2``
     - `typescript <https://www.typescriptlang.org/>`_

----

6.2.0 (05/11/2021)
------------------

Dependency Updates
^^^^^^^^^^^^^^^^^^

.. list-table::
   :header-rows: 1

   * - Dependency
     - Status
     - Version
   * - ``@viasoft/app-core``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/client``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/client-core``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/common``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/components``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/http``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/navigation``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/theming``
     - 🔃 Updated
     - ``6.3.4``
   * - ``@viasoft/view-template``
     - 🔃 Updated
     - ``6.3.4``


Breaking Changes
^^^^^^^^^^^^^^^^


* `FRM-1857 <http://jira.korp.com.br/browse/FRM-1857>`_\ : Removed ``VS_FILE_PROVIDER_PREFIX`` and replaced its usages with ``VS_API_PREFIX``.
