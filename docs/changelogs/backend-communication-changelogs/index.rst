Backend Communication Changelogs
================================

Changelogs de alterações feitas no Backend que afetam o Frontend

.. toctree:: 
    :maxdepth: 1

    Chamadas com Versionamento (01/05/2022) <2022-05-01-versioned-routes>
