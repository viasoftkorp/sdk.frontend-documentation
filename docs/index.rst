
Bem-Vindo ao Viasoft SDK
========================

O Viasoft SDK Frontend é um framework para aplicações Frontend que se integram com Viasoft Korp Portal Core. 

Com ele é póssível criar aplicações robustas, de forma simples. Ele foi construído em conjunto com o Viasoft SDK Backend, que é um poderoso backend em .NetCore3 baseado em micro serviços;

Quando devo usar @Viasoft/Frontend?
-----------------------------------

* Quando for criar uma aplicação para Viasoft Korp Portal.
* Quando for criar uma aplicação Angular que integra com Viasoft Korp Portal.

Contents
--------

.. toctree::
	:glob:

	changelogs/*
	components/*
	design/*
	error-monitoring/*
	libraries/*
	testing/*
	user-guide/*