
Instalação - Testing
====================

Para instalar o pacote, navegue até seu projeto, abra o terminal e execute o comando ``npm i --save-dev @viasoft/testing``.

Para utilizá-lo, execute o comando ``npx vs-test``. Se alguma configuração precisa ser feita no projeto, ela será executada automaticamente e deve ser salva pelo desenvolvedor.

Ao utilizá-la para executar ou atualizar testes, é preciso que a aplicação esteja rodando paralelamente aos testes.

Para mais informações, execute o comando ``npx vs-test --help``.
